#!/bin/bash

# Define namespace and resources to back up
NAMESPACE="myapp-namespace"
DEPLOYMENT="myapp-db"
BACKUP_PATH="/path/to/backup"

# Date for filename
DATE=$(date +%Y%m%d%H%M)

# Create backup directory if not exists
mkdir -p "$BACKUP_PATH"

# Export deployment YAML
kubectl get deployment "$DEPLOYMENT" -n "$NAMESPACE" -o yaml > "$BACKUP_PATH/$DEPLOYMENT-backup-$DATE.yaml"

echo "Backup completed for deployment: $DEPLOYMENT"
