#!/bin/bash

# Define primary and secondary service endpoints
PRIMARY_SERVICE="myapp-service-primary"
SECONDARY_SERVICE="myapp-service-secondary"
NAMESPACE="myapp-namespace"

# Check primary service status
if kubectl get svc "$PRIMARY_SERVICE" -n "$NAMESPACE" &> /dev/null; then
    echo "Primary service is up... No failover needed."
else
    echo "Primary service is down... Initiating failover to secondary service."
    
    # Redirect all traffic to secondary service by modifying the service selector
    kubectl patch svc "$PRIMARY_SERVICE" -n "$NAMESPACE" --type='json' -p='[{"op": "replace", "path": "/spec/selector", "value": {"app": "myapp-secondary"}}]'
    
    echo "Failover complete. Traffic redirected to secondary service."
fi
