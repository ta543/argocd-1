#!/bin/bash

# Check if required parameters are passed
if [ "$#" -ne 2 ]; then
    echo "Usage: ./deploy.sh <application-name> <environment>"
    exit 1
fi

APP_NAME=$1
ENVIRONMENT=$2

# Sync the ArgoCD app
echo "Deploying $APP_NAME to $ENVIRONMENT..."
argocd app sync $APP_NAME --label environment=$ENVIRONMENT

# Check the deployment status
STATUS=$(argocd app get $APP_NAME --output json | jq -r '.status.sync.status')
if [ "$STATUS" == "Synced" ]; then
    echo "Deployment of $APP_NAME to $ENVIRONMENT was successful."
else
    echo "Deployment failed, please check ArgoCD for more details."
    exit 1
fi
