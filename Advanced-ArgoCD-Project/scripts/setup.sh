#!/bin/bash

# Define the namespace to be created
NAMESPACE="myapp-namespace"

# Check if the namespace exists
if kubectl get namespace "$NAMESPACE" > /dev/null 2>&1; then
    echo "Namespace '$NAMESPACE' already exists."
else
    echo "Creating namespace '$NAMESPACE'..."
    kubectl create namespace "$NAMESPACE"
    if [ $? -eq 0 ]; then
        echo "Namespace '$NAMESPACE' created successfully."
    else
        echo "Failed to create namespace '$NAMESPACE'."
        exit 1
    fi
fi

